console.log('Hello, World')
// Details
const details = {
    firstName: "Ron",
    lastName: "Celajes",
    age: 19,
    hobbies : [
        "sports", "motorcycle riding", "gaming"
    ] ,
    address: {
        housenumber: "B19 L34",
        street: "John Street",
        city: "Caloocan City",
        state: "Metro Manila",
    }
}
const address = Object.values(details.address);
console.log("My First Name is " + details.firstName);
console.log("My Last Name is " + details.lastName);
console.log(`Yes, I am ${details.firstName} ${details.lastName}.`);
console.log("I am " + details.age + " years old.");
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I live at " + address.join(", ") + ".");
